# p-meilisearch

#### 介绍
p-meilisearch 是基于 meilisearch/meilisearch-php 开发的进一步对接meilisearch的工具

#### 软件架构

-src&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;根目录  
---common &emsp; &emsp; 公共方法   
---exception  &emsp;&emsp;异常定义     
---lib   
------factory &emsp;&emsp; 工厂对象  
------index  &emsp;&emsp;&emsp;meilisearch操作  
------key  &emsp;&emsp;&emsp;&emsp;秘钥interface  
------table&emsp; &emsp; &emsp;文档interface             
---message &emsp;&emsp; 定义参数信息


#### 安装教程
composer require pcl/p-meilisearch

#### 使用说明
```
实例化 MeiLiService
$MeiLiService = new MeiLiService($client_url,$client_apiKey);
```
######1.索引管理
```
---获取所有索引
$MeiLiService->getAllIndexes();
---获取指定索引信息
$MeiLiService->getIndexDetail($index);
---创建索引
$MeiLiService->createIndex(TableInterFace $table);
---更新索引
$MeiLiService->updateIndex(TableInterFace $table);
---删除索引
$MeiLiService->deleteIndex(TableInterFace $table);
```

######2.文件管理
```
默认提供了 TableAbstract 来实现了 TableInterFace,使用过程中,只需要继承 TableAbstract即可快速添加一个文档信息

---添加文档
$MeiLiService->addData(TableInterFace $table);
---修改文档
$MeiLiService->editData(TableInterFace $table);
---删除单个文档
$MeiLiService->delData(string $index,$documentId);
---删除多个文档
$MeiLiService->delMoreData(string $index,array $documentId);
---删除所有文档
$MeiLiService->delAllData(TableInterFace $table);
```
######3.文件查询
```
默认提供了 TableAbstract 来实现了 TableInterFace,使用过程中,只需要继承 TableAbstract即可快速添加一个文档信息

---获取指定文档信息
$MeiLiService->getData(TableInterFace $table,$documentId);
---获取指定文档信息
$MeiLiService->getData(TableInterFace $table,PostSearchMessage $PostSearchMessage,string $keyword,array $options = []);
```
######4.异步任务
```
---获取异步任务详情
$MeiLiService->getTask($uid = null);
---获取指定任务的处理结果
$MeiLiService->getTaskResult($uid);
```
######5.秘钥管理
```
默认提供了 KeyAbstract 来实现了 KeyInterface,使用过程中,只需要继承 KeyAbstract即可快速添加一个秘钥信息

---获取所有密钥
$MeiLiService->getKeys($uid = null);
---获取指定秘钥内容
$MeiLiService->getKey(string $key_uid);
---创建秘钥
$MeiLiService->createKey(KeyInterface $key);
---修改秘钥
$MeiLiService->updateKey(KeyInterface $key);
---删除秘钥
$MeiLiService->deleteKey(string $key_uid);
```

