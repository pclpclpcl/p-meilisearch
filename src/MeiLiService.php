<?php
namespace Pmeilisearch;

use app\common\lib\key\KeyInterface;
use MeiliSearch\Client;
use Pmeilisearch\common\Pfactory;
use Pmeilisearch\exception\IndexException;
use Pmeilisearch\lib\index\AsyncTask;
use Pmeilisearch\lib\index\ManageData;
use Pmeilisearch\lib\index\ManageIndex;
use Pmeilisearch\lib\index\ManageKeys;
use Pmeilisearch\lib\index\SearchData;
use Pmeilisearch\lib\SearchResult;
use Pmeilisearch\message\KeyDataMessage;
use Pmeilisearch\message\PostSearchMessage;
use Pmeilisearch\message\TaskDataMessage;
use Pmeilisearch\lib\table\TableInterFace;
/**
 * Class MeiLiService
 * @package Pmeilisearch
 *
 * 使用流程：
 *
 * 1、索引管理 --
 * @method \MeiliSearch\Endpoints\Indexes[] getAllIndexes() 获取所有索引
 * @method array|null getIndexDetail($index) 获取指定索引信息
 * @method array createIndex(TableInterFace $table) 创建索引
 * @method array updateIndex(TableInterFace $table) 更新索引
 * @method bool deleteIndex(TableInterFace $table) 删除索引
 * 2、文件管理 --
 * @method TaskDataMessage|bool addData(TableInterFace $table) 添加文档
 * @method TaskDataMessage|bool editData(TableInterFace $table) 修改文档
 * @method TaskDataMessage|bool delData(string $index,$documentId) 删除单个文档
 * @method TaskDataMessage delMoreData(string $index,array $documentId) 删除多个文档
 * @method TaskDataMessage|bool delAllData(TableInterFace $table) 删除所有文档
 * 3、文件查询 --
 * @method TableInterFace|bool getData(TableInterFace $table,$documentId) 获取指定文档信息
 * @method SearchResult|array getSearch(TableInterFace $table,PostSearchMessage $PostSearchMessage,string $keyword,array $options = []) 获取指定文档信息
 * 4、 异步任务 --
 * @method array getTask($uid = null) 获取异步任务详情
 * @method array getTaskResult($uid) 获取指定任务的处理结果
 * 5、 秘钥管理 --
 * @method array getKeys() 获取所有密钥
 * @method KeyDataMessage getKey(string $key_uid) 获取指定秘钥内容
 * @method KeyDataMessage createKey(KeyInterface $key) 创建秘钥
 * @method KeyDataMessage updateKey(KeyInterface $key) 修改秘钥
 * @method array deleteKey(string $key_uid) 删除秘钥
 */
class MeiLiService
{
    protected $client_url = '';

    protected $client_apiKey = '';

    protected $methods = [
        //索引管理
        ManageIndex::class=>['getAllIndexes', 'getIndexDetail', 'createIndex', 'updateIndex', 'deleteIndex'],
        //文档数据管理
        ManageData::class=>['addData', 'editData', 'delData', 'delMoreData', 'delAllData'],
        //数据查询
        SearchData::class=>['getData', 'getMoreData', 'getSearch'],
        //异步任务
        AsyncTask::class=>['getTask','getTaskResult'],
        //秘钥管理
        ManageKeys::class=>['getKeys','getKey','createKey','updateKey','deleteKey']
    ];

    /**
     * @var Client|null
     */
    protected $Client = null;

    public function __construct($client_url = '',$client_apiKey = '')
    {
        $this->init($client_url,$client_apiKey);
        //翻转
        foreach ($this->methods AS &$v_methods){
            $v_methods = array_flip($v_methods);
        }
    }

    /**
     * 初始化
     * @param string $client_url
     * @param string $client_apiKey
     * author PengChengLei time 2022-08-10 09:34:03
     * @return $this
     */
    public function init($client_url = '',$client_apiKey = ''){
        $this->client_url = $client_url;
        $this->client_apiKey = $client_apiKey;
        $this->Client = Pfactory::getInstance()->ClientFactory->getClient($this->client_url,$this->client_apiKey);
        return $this;
    }

    /**
     * 获取连接
     * author PengChengLei time 2022-08-10 09:33:56
     * @return Client
     */
    public function getClient(){
        if(is_null($this->Client)){
            //初始化
            $this->init();
        }
        return clone $this->Client;
    }

    public function __call($name, $arguments)
    {
        $class = null;
        foreach ($this->methods AS $k_class=>$v_methods){
            if(isset($v_methods[$name])){
                $class = $k_class;
                break;
            }
        }
        if(is_null($class)){
            throw new IndexException('调用方法错误');
        }
        array_unshift($arguments,$this->getClient());
        return call_user_func_array([new $class(),$name],$arguments);
    }
}