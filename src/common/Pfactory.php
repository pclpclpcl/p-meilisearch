<?php


namespace Pmeilisearch\common;

use Pmeilisearch\exception\SearchException;
use Pmeilisearch\lib\factory\ClientFactory;
use Pmeilisearch\lib\factory\MessageFactory;
use Pmeilisearch\lib\factory\TableFactory;

/**
 * Class Pfactory
 * @package Pmeilisearch\lib
 * @property ClientFactory ClientFactory
 * @property MessageFactory MessageFactory
 * @property TableFactory TableFactory
 */
class Pfactory
{
    /**
     * @var Pfactory
     */
    protected static $instance = null;

    /**
     * @var array
     */
    protected $drivers = [];

    public function __get($name)
    {
        $class = null;
        switch ($name){
            //获取连接
            case 'ClientFactory':
                $class = ClientFactory::class;
                break;
            //task和search请求对象
            case 'MessageFactory':
                $class = MessageFactory::class;
                break;
            //表单模板
            case 'TableFactory':
                $class = TableFactory::class;
                break;
            default:
                break;
        }
        if(is_null($class)){
            throw new SearchException('未知的表单信息');
        }
        if(!isset($this->drivers[$name])){
            $this->drivers[$name] = new $class();
        }
        return $this->drivers[$name];
    }

    /**
     * @param bool $force '强制重新获取'
     * author PengChengLei time 2022-07-20 14:42:55
     * @return Pfactory
     */
    public static function getInstance($force = false){
        if(is_null(self::$instance) || $force){
            self::$instance = new static();
        }
        return self::$instance;
    }
}