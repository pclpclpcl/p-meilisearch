<?php


namespace Pmeilisearch\exception;

use Exception;

/**
 * Class IndexException
 * @package meilisearch\exception
 */
class IndexException extends Exception
{

}