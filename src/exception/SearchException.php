<?php


namespace Pmeilisearch\exception;

use Exception;

/**
 * Class SearchException
 * @package meilisearch\exception
 */
class SearchException extends Exception
{

}