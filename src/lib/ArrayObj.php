<?php



namespace Pmeilisearch\lib;

use ArrayAccess;

class ArrayObj implements ArrayAccess
{
    /**
     * @var array
     */
    private $data = [];

    /**
     * 是否校验字段
     * @var bool
     */
    private $is_check = false;

    /**
     * 需要校验的字段
     * @var array
     */
    protected $check_field = [];

    /**
     * 必填字段(字段必须存在,且不能为null)
     * @var array
     */
    protected $must_field = [];

    /**
     * 非空字段(字段不能为空、0和null)
     * @var array
     */
    protected $not_empty_field = [];

    /**
     * ArrayObj constructor.
     */
    public function __construct()
    {
        $this->init();
    }

    /**
     * 初始化
     * author PengChengLei time 2021-12-02 14:51:57
     */
    protected function init()
    {
        $this->open_check();
    }

    /**
     * 开启校验
     * author PengChengLei time 2021-12-09 14:33:07
     */
    protected function open_check()
    {
        $this->is_check = true;
    }

    /**
     * 关闭校验
     * author PengChengLei time 2021-12-09 14:33:07
     */
    protected function close_check()
    {
        $this->is_check = false;
    }

    /**
     * 返回数据
     * name getData
     * add PengChengLei
     * time 2021-08-18 14:59:27
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * 清空数据
     * author PengChengLei time 2021-12-02 15:10:47
     * @return $this
     */
    public function clean_data()
    {
        $this->data = [];
        return $this;
    }

    /**
     * 校验必填字段
     * author PengChengLei time 2021-12-02 14:59:27
     */
    public function check_must()
    {
        //必填验证
        foreach ($this->must_field as $k_data => $v_data) {
            $da = $this->getAttr($k_data);
            if (is_null($da)) {
                return ['code' => 0, 'msg' => $v_data];
            }
        }
        //非空验证
        foreach ($this->not_empty_field as $k_empty_field => $v_empty_field) {
            $da = $this->getAttr($k_empty_field);
            if (is_null($da)) {
                return ['code' => 0, 'msg' => $v_empty_field];
            }
        }
        return ['code' => 1, 'msg' => '校验通过'];
    }

    /**
     * 获取数据
     * @param $name
     * @param null $default
     * author PengChengLei time 2021-12-02 14:53:22
     * @return mixed|null
     */
    private function getAttr($name, $default = null)
    {
        return isset($this->data[$name]) ? $this->data[$name] : $default;
    }

    /**
     * 批量设置数据
     * @param array $data
     * author PengChengLei time 2021-12-02 14:54:10
     * @return $this
     */
    public function setAttrs(array $data)
    {
        foreach ($data as $key => $val) {
            $this->setAttr($key, $val);
        }
        return $this;
    }

    /**
     * 单独设置数据
     * @param $name
     * @param $value
     * author PengChengLei time 2021-12-02 14:53:33
     * @return $this
     */
    public function setAttr($name, $value)
    {
        if ($this->is_check) {
            if (in_array($name, $this->check_field)) {
                $this->data[$name] = $value;
            }
        } else {
            $this->data[$name] = $value;
        }

        return $this;
    }

    /**
     * 验证是否存在
     * @param $name
     * author PengChengLei time 2021-12-02 14:54:36
     * @return bool
     */
    public function __isset($name)
    {
        return !is_null($this->getAttr($name));
    }

    /**
     * 移除数据
     * @param $name
     * author PengChengLei time 2021-12-02 14:54:46
     */
    public function __unset($name)
    {
        unset($this->data[$name]);
    }

    /**
     * @inheritDoc
     */
    public function offsetExists($name)
    {
        return $this->__isset($name);
    }

    /**
     * @inheritDoc
     */
    public function offsetGet($name)
    {
        return $this->getAttr($name);
    }

    /**
     * @inheritDoc
     */
    public function offsetSet($name, $value)
    {
        $this->setAttr($name, $value);
    }

    /**
     * @inheritDoc
     */
    public function offsetUnset($name)
    {
        $this->__unset($name);
    }
}