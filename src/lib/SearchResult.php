<?php

namespace Pmeilisearch\lib;

use MeiliSearch\Search\SearchResult AS ComposerSearchResult;

/**
 * 搜索结果 -- 依赖 meilisearch/meilisearch-php
 * Class SearchResult
 * @package meilisearch\lib
 */
class SearchResult extends ComposerSearchResult{
    public function __construct(array $body)
    {
        $body['nbHits'] = $body['nbHits'] ?? 0;
        parent::__construct($body);
    }
}