<?php


namespace Pmeilisearch\lib\factory;

use MeiliSearch\Client;

/**
 * 获取连接
 * Class ClientFactory
 * @package app\common\meilisearch\lib
 */
class ClientFactory
{

    /**
     * @var array
     */
    protected $clients = [];

    protected $client_url = 'http://127.0.0.1:7700';

    protected $client_apiKey = 'masterKey';

    /**
     * 获取连接
     * author PengChengLei time 2022-07-20 14:39:58
     * @param string $client_url
     * @param string $client_apiKey
     * @param bool $force
     * @return Client
     */
    public function getClient($client_url = '',$client_apiKey = '',$force = false){
        /* @var $client Client*/
        $client = $this->createClient($client_url.$client_apiKey,$force);
        return clone $client;
    }

    /**
     * @param string $client_url
     * @param string $client_apiKey
     * @param bool $force
     * @return $this
     */
    protected function createClient($client_url = '',$client_apiKey = '',$force = false){
        $client_url = empty($client_url) ? $this->client_url : $client_url;
        $client_apiKey = empty($client_apiKey) ? $this->client_apiKey : $client_apiKey;
        $key = sha1($client_url . '-' . $client_apiKey);
        if($force || !isset($this->clients[$key])){
            $this->clients[$key] = new Client($client_url, $client_apiKey);
        }
        return $this->clients[$key];
    }
}