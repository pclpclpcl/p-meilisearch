<?php


namespace Pmeilisearch\lib\factory;


use Pmeilisearch\exception\SearchException;
use Pmeilisearch\lib\ArrayObj;
use Pmeilisearch\message\KeyDataMessage;
use Pmeilisearch\message\PostSearchMessage;
use Pmeilisearch\message\TaskDataMessage;

/**
 *
 * Class MessageFactory
 * @package app\common\meilisearch\lib
 * @property PostSearchMessage PostSearchMessage
 * @property TaskDataMessage TaskDataMessage
 * @property KeyDataMessage KeyDataMessage
 */
class MessageFactory
{
    protected $drivers = [];
    /**
     * 获取表单
     * @param $name
     * author PengChengLei time 2022-07-20 14:46:04
     * @return ArrayObj
     * @throws SearchException
     */
    public function __get($name)
    {
        $class = null;
        switch ($name){
            case 'PostSearchMessage':
                $class = PostSearchMessage::class;
                break;
            case 'TaskDataMessage':
                $class = TaskDataMessage::class;
                break;
            case 'KeyDataMessage':
                $class = KeyDataMessage::class;
                break;
            default:
                break;
        }
        if(is_null($class)){
            throw new SearchException('未知的表单信息');
        }
        if(!isset($this->drivers[$name])){
            $this->drivers[$name] = new $class();
        }
        return clone $this->drivers[$name];
    }
}