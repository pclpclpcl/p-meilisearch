<?php


namespace Pmeilisearch\lib\factory;

use Pmeilisearch\exception\SearchException;
use Pmeilisearch\lib\table\TableAbstract;
use Pmeilisearch\lib\table\TableInterFace;

/**
 * 表单工厂
 * Class TableFactory
 * @package app\common\meilisearch\lib
 */
class TableFactory
{

    private $namespace = 'Pmeilisearch\lib\table\files\\';

    protected $drivers = [];

    /**
     * 获取表单
     * @param $name
     * @return TableInterFace
     * @throws SearchException
     * author PengChengLei time 2022-07-20 14:46:04
     */
    public function getTable($name)
    {
        //命名空间下 -- 默认配置信息
        $class = $this->namespace . $name;
        if(class_exists($class)){
            $class = $this->create_table($class);
            return $class->init();
        }
        //指定table表
        if(class_exists($name)){
            $class = $this->create_table($name);
            return $class->init();
        }
        throw new SearchException('未知的表单信息');
    }

    /**
     * @param $class
     * @return mixed|TableAbstract
     * @throws SearchException
     */
    protected function create_table($class){
        try {
            if(!isset($this->drivers[$class])){
                $this->drivers[$class] = new $class();
            }
        }catch (\Throwable $exception){
            throw new SearchException('未知的表单信息');
        }
        $class = $this->drivers[$class];
        if(!$class instanceof TableInterFace){
            throw new SearchException('表单信息必须实现 TableInterFace');
        }
        return $class;
    }
}