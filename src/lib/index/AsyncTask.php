<?php
namespace Pmeilisearch\lib\index;

use MeiliSearch\Client;
use Pmeilisearch\exception\TaskException;

/**
 * 异步任务查询
 * Class AsyncTask
 */
class AsyncTask
{
    /**
     * 获取异步任务详情
     * @param Client $client
     * @param null $uid
     * author PengChengLei time 2022-08-09 17:15:34
     * @return array
     */
    public static function getTask(Client $client,$uid = null){
        if(is_null($uid)){
            return $client->getTasks();
        }
        return $client->getTask($uid);
    }

    /**
     * 获取指定任务的处理结果
     * @param Client $client
     * @param $uid
     * author PengChengLei time 2022-08-09 17:16:41
     * @return array
     * @throws TaskException
     */
    public static function getTaskResult(Client $client,$uid){
        $result = self::getTask($client,$uid);
        if(isset($result['status'])){
            switch ($result['status']){
                //任务成功
                case 'succeeded':
                    //任务失败
                case 'failed':
                    return $result;
                    break;
                default:
                    sleep(2);
                    return self::getTaskResult($client,$uid);
                    break;
            }
        }
        throw new TaskException('没有找到任务信息');
    }
}