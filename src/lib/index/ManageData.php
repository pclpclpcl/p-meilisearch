<?php


namespace Pmeilisearch\lib\index;

use MeiliSearch\Client;
use Pmeilisearch\common\Pfactory;
use Pmeilisearch\message\TaskDataMessage;
use Pmeilisearch\lib\table\TableInterFace;

/**
 * 文档内容管理
 * Class ManageData
 * @package Pmeilisearch\lib\index
 */
class ManageData
{
    /**
     * 添加文档
     * @param Client $client
     * @param TableInterFace $table
     * @return TaskDataMessage|bool
     */
    public function addData(Client $client,TableInterFace $table){
        if($table->checkData()){
            $result = $client->index($table->getIndex())->addDocuments($table->getData(),$table->getPrimaryKey());
            $TaskMessage = Pfactory::getInstance()->MessageFactory->TaskDataMessage;
            $TaskMessage->setAttrs($result);
            return $TaskMessage;
        }
        return false;
    }

    /**
     * 修改文档
     * @param Client $client
     * @param TableInterFace $table
     * author PengChengLei time 2022-07-20 15:13:21
     * @return TaskDataMessage|bool
     */
    public function editData(Client $client,TableInterFace $table){
        if($table->checkData()){
            $result = $client->index($table->getIndex())->updateDocuments($table->getData(),$table->getPrimaryKey());
            $TaskMessage = Pfactory::getInstance()->MessageFactory->TaskDataMessage;
            $TaskMessage->setAttrs($result);
            return $TaskMessage;
        }
        return false;
    }

    /**
     * 删除单个文档
     * @param Client $client
     * @param string $index
     * @param $documentId
     * author PengChengLei time 2022-07-20 15:23:37
     * @return TaskDataMessage|bool
     */
    public function delData(Client $client,string $index,$documentId){
        $result = $client->index($index)->deleteDocument($documentId);
        /* @var */
        $TaskMessage = Pfactory::getInstance()->MessageFactory->TaskDataMessage;
        $TaskMessage->setAttrs($result);
        return $TaskMessage;
    }

    /**
     * 删除多个文档
     * @param Client $client
     * @param string $index
     * @param array $documentIds
     * author PengChengLei time 2022-07-20 15:23:45
     * @return TaskDataMessage
     */
    public function delMoreData(Client $client,string $index,array $documentIds){
        $result = $client->index($index)->deleteDocuments($documentIds);
        $TaskMessage = Pfactory::getInstance()->MessageFactory->TaskDataMessage;
        $TaskMessage->setAttrs($result);
        return $TaskMessage;
    }

    /**
     * 删除所有文档
     * @param Client $client
     * @param TableInterFace $table
     * author PengChengLei time 2022-07-20 15:23:53
     * @return TaskDataMessage|bool
     */
    public function delAllData(Client $client,TableInterFace $table){
        if($table->checkData('index')){
            $result = $client->index($table->getIndex())->deleteAllDocuments();
            $TaskMessage = Pfactory::getInstance()->MessageFactory->TaskDataMessage;
            $TaskMessage->setAttrs($result);
            return $TaskMessage;
        }
        return false;
    }
}