<?php
namespace Pmeilisearch\lib\index;

use MeiliSearch\Client;
use Pmeilisearch\exception\IndexException;
use Pmeilisearch\exception\TaskException;
use Pmeilisearch\lib\table\TableInterFace;

/**
 * 管理索引及属性
 * Class ManageIndex
 */
class ManageIndex
{
    /**
     * 获取所有索引
     * author PengChengLei time 2022-08-09 16:37:09
     * @param Client $client
     * @return \MeiliSearch\Endpoints\Indexes[]
     */
    public function getAllIndexes(Client $client){
        return $client->getAllIndexes();
    }

    /**
     * 获取指定索引信息
     * author PengChengLei time 2022-08-09 16:59:20
     * @param Client $client
     * @param $index
     * @return array|null
     * //示例
     * [
     * "uid" => "user"
     * "createdAt" => "2022-07-20T08:30:53.0967886Z"
     * "updatedAt" => "2022-07-20T08:30:53.1135365Z"
     * "primaryKey" => "id"
     * ]
     */
    public function getIndexDetail(Client $client,$index){
        return $client->index($index)->fetchRawInfo();
    }

    /**
     * 创建索引
     * author PengChengLei time 2022-08-09 17:00:25
     * @param Client $client
     * @param TableInterFace $table
     * @return array
     * @throws IndexException
     * @throws TaskException
     */
    public function createIndex(Client $client,TableInterFace $table){
        try {
            $this->getIndexDetail($client,$table->getIndex());
        }catch (\Throwable $exception){
            throw new IndexException('创建索引失败,索引已存在');
        }
        $result = $client->createIndex($table->getIndex(),['primaryKey'=>$table->getPrimaryKey()]);
        //获取执行情况
        if(isset($result['taskUid'])){
            return AsyncTask::getTaskResult($client,$result['taskUid']);
        }
        throw new IndexException('创建索引失败');
    }

    /**
     * 更新索引
     * @param Client $client
     * @param TableInterFace $table
     * author PengChengLei time 2022-08-10 09:40:16
     * @return array
     * @throws IndexException 更新索引的 主键。索引uid是必需的。
     *只要索引不包含文档，您就可以自由更新索引的主键。
     *要更改已包含文档的索引的主键，您必须首先删除该索引中的所有文档。然后您可以更改主键，最后再次索引您的数据集
     * @throws TaskException
     */
    public function updateIndex(Client $client,TableInterFace $table){
        try {
            $this->getIndexDetail($client,$table->getIndex());
        }catch (\Throwable $exception){
            throw new IndexException('创建索引失败,索引已存在');
        }
        $result = $client->index($table->getIndex())->update(['primaryKey'=>$table->getPrimaryKey()]);
        //获取执行情况
        if(isset($result['taskUid'])){
            return AsyncTask::getTaskResult($client,$result['taskUid']);
        }
        throw new IndexException('更新索引失败');
    }

    /**
     * 删除索引
     * author PengChengLei time 2022-08-10 09:45:18
     * @param Client $client
     * @param TableInterFace $table
     * @return bool
     * @throws IndexException
     * @throws TaskException
     */
    public function deleteIndex(Client $client,TableInterFace $table){
        try {
            $this->getIndexDetail($client,$table->getIndex());
        }catch (\Throwable $exception){
            return true;
        }
        $result = $client->index($table->getIndex())->delete();
        //获取执行情况
        if(isset($result['taskUid'])){
            $res = AsyncTask::getTaskResult($client,$result['taskUid']);
            if($res['status'] == 'succeeded'){
                return true;
            }
        }
        throw new IndexException('删除索引失败');
    }
}