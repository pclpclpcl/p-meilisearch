<?php


namespace Pmeilisearch\lib\index;

use app\common\lib\key\KeyInterface;
use MeiliSearch\Client;
use Pmeilisearch\common\Pfactory;
use Pmeilisearch\exception\KeyException;

/**
 * 秘钥管理工具
 * Class ManageKeys
 * @package Pmeilisearch\lib\index
 */
class ManageKeys
{
    /**
     * 获取所有密钥
     * @param Client $client
     * author PengChengLei time 2022-08-10 10:55:13
     * @return array
     */
    public function getKeys(Client $client){
        return $client->getKeys();
    }

    /**
     * 获取指定秘钥内容
     * @param Client $client
     * @param string $key_uid
     * author PengChengLei time 2022-08-10 11:25:51
     * @return \Pmeilisearch\lib\ArrayObj|\Pmeilisearch\message\KeyDataMessage
     */
    public function getKey(Client $client,string $key_uid){
        $result = $client->getKey($key_uid);
        $keyData = Pfactory::getInstance()->MessageFactory->KeyDataMessage;
        $keyData->setAttrs($result);
        return $keyData;
    }

    /**
     * 创建秘钥
     * author PengChengLei time 2022-08-10 11:27:01
     * @param Client $client
     * @param KeyInterface $key
     * @return \Pmeilisearch\lib\ArrayObj|\Pmeilisearch\message\KeyDataMessage
     */
    public function createKey(Client $client,KeyInterface $key){
        $result = $client->createKey($key->getAll());
        $keyData = Pfactory::getInstance()->MessageFactory->KeyDataMessage;
        $keyData->setAttrs($result);
        return $keyData;
    }

    /**
     * 修改秘钥
     * @param Client $client
     * @param KeyInterface $key
     * author PengChengLei time 2022-08-10 14:20:29
     * @return \Pmeilisearch\lib\ArrayObj|\Pmeilisearch\message\KeyDataMessage
     * @throws KeyException
     */
    public function updateKey(Client $client,KeyInterface $key){
        $uid = $key->getUid();
        if(is_null($uid)){
            throw new KeyException('未知的秘钥id');
        }
        $result = $client->updateKey($uid,$key->getAll());
        $keyData = Pfactory::getInstance()->MessageFactory->KeyDataMessage;
        $keyData->setAttrs($result);

        return $keyData;
    }

    /**
     * 删除秘钥
     * @param Client $client
     * @param string $key_uid
     * author PengChengLei time 2022-08-10 14:21:07
     * @return array
     */
    public function deleteKey(Client $client,string $key_uid){
        return $client->deleteKey($key_uid);
    }
}