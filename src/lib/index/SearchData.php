<?php


namespace Pmeilisearch\lib\index;

use MeiliSearch\Client;
use Pmeilisearch\lib\SearchResult;
use Pmeilisearch\message\PostSearchMessage;
use Pmeilisearch\lib\table\TableInterFace;

class SearchData
{
    /**
     * 获取指定文档信息
     * @param Client $client
     * @param TableInterFace $table
     * @param $documentId
     * author PengChengLei time 2022-07-20 16:37:57
     * @return TableInterFace|bool
     */
    public function getData(Client $client,TableInterFace $table,$documentId){
        if($table->checkData('index')){
            try {
                $result = $client->index($table->getIndex())->getDocument($documentId);
                $table->setData($result);
                return $table;
            }catch (\Throwable $exception){
                return false;
            }
        }
        return false;
    }

    /**
     * 获取文档中的所有内容
     * @param Client $client
     * @param TableInterFace $table
     * author PengChengLei time 2022-07-20 16:40:08
     * @param int $offset
     * @param int $limit
     * @param string $fields
     * @return array
     */
    public function getMoreData(Client $client,TableInterFace $table,$offset = 0,$limit = 20,$fields = '*'){
        try {
            $result = $client->index($table->getIndex())->getDocuments([
                'offset'=>$offset,
                'limit'=>$limit,
                'fields'=>$fields
            ]);
        }catch (\Throwable $exception){
            $result = [
                'results'=>[],
                'offset'=>0,
                'limit'=>$limit,
                'total'=>0
            ];
        }
        return $result;
    }

    /**
     * 搜索文档信息
     * author PengChengLei time 2022-07-20 17:18:06
     * @param Client $client
     * @param TableInterFace $table
     * @param PostSearchMessage $PostSearchMessage
     * @param string $keyword
     * @param array $options
     * @return SearchResult|array
     */
    public function getSearch(Client $client,TableInterFace $table,PostSearchMessage $PostSearchMessage,string $keyword,array $options = []){
        $result = $client->index($table->getIndex())->rawSearch($keyword,$PostSearchMessage->getData());
        if (\array_key_exists('raw', $options) && $options['raw']) {
            return $result;
        }
        $searchResult = new SearchResult($result);
        $searchResult->applyOptions($options);
        return $searchResult;
    }
}