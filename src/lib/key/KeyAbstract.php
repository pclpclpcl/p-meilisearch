<?php
namespace app\common\lib\key;

use Pmeilisearch\exception\KeyException;
use Pmeilisearch\lib\ArrayObj;

/**
 * Class KeyAbstract
 * @package app\common\lib\key
 * @property string name 秘钥名称
 * @property string uid 秘钥uid
 * @property string description 秘钥备注
 * @property array actions 秘钥配置 ['*']表示所有动作都允许
 * @property array indexes 秘钥允许操作数组 ['*']表示都可以操作
 * @property null|string expiresAt 秘钥过期时间 为 null 时表示永不过期
 * @method KeyInterface setSearch()
 * @method KeyInterface delSearch()
 * @method KeyInterface setDocuments($name)
 * @method KeyInterface delDocuments($name = null)
 * @method KeyInterface setIndexes($name)
 * @method KeyInterface delIndexes($name = null)
 * @method KeyInterface setTasks($name)
 * @method KeyInterface delTasks($name = null)
 * @method KeyInterface setSettings($name)
 * @method KeyInterface delSettings($name = null)
 * @method KeyInterface setStats($name)
 * @method KeyInterface delStats($name = null)
 * @method KeyInterface setDumps($name)
 * @method KeyInterface delDumps($name = null)
 * @method KeyInterface setVersion()
 * @method KeyInterface delVersion()
 * @method KeyInterface setKeys($name)
 * @method KeyInterface delKeys($name = null)
 */
abstract class KeyAbstract extends ArrayObj implements KeyInterface
{
    protected $default_data = [
        'name'=>null,
        'uid'=>null,
        'description'=>'',
        'actions'=>['*'],
        'indexes'=>['*'],
        'expiresAt'=>null
    ];

    public function __construct()
    {
        parent::__construct();
        $this->close_check();
        $this->setAttrs($this->default_data);
    }

    public function setName($name){
        $this->setAttr('name',$name);
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setUid($uid){
        $this->setAttr('uid',$uid);
        return $this;
    }

    public function getUid()
    {
        return $this->uid;
    }

    public function setDescription($Description)
    {
        $this->setAttr('description',$Description);
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setExpiresAt($ExpiresAt)
    {
        $this->setAttr('expiresAt',$ExpiresAt);
        return $this;
    }

    public function getExpiresAt()
    {
        return $this->expiresAt;
    }

    public function getAll()
    {
        return $this->getData();
    }

    public function cleanAll(){
        return $this->clean_data();
    }

    public function __call($name, $arguments = '')
    {
        $actions_field = null;
        $actions_fields = [
            'search',
            'documents',
            'indexes',
            'tasks',
            'settings',
            'stats',
            'dumps',
            'version',
            'keys'
        ];
        $type = substr($name,0,3);
        $actions_field = strtolower(substr($name,3,mb_strlen($name)));
        if(!in_array($actions_field,$actions_fields)){
            throw new KeyException('未知的方法:'.$name);
        }
        switch ($type){
            case 'set':
                $this->setActions($actions_field,$arguments);
                return $this;
                break;
            case 'del':
                $this->delActions($actions_field,$arguments);
                return $this;
                break;
            case 'get':
                return $this->getActions($actions_field);
                break;
            default:
                break;
        }
        throw new KeyException('调用错误');
    }

    /**
     * 重置默认值
     * author PengChengLei time 2022-08-10 13:35:16
     * @param string $filed
     */
    public function resetData($filed = 'actions'){
        if(isset($this->default_data[$filed])){
            $this->setAttr($filed,$this->default_data[$filed]);
        }
    }

    /**
     * 设置actions配置
     * @param $key
     * @param $value
     * author PengChengLei time 2022-08-10 13:30:56
     */
    protected function setActions($key,$value){
        $now_actions = $this->actions;
        if(in_array('*',$now_actions)){
            $now_actions = [];
        }
        $f_key = $this->build_temp_key($key,$value);
        $now_actions = array_values(array_filter(array_unique(array_merge($now_actions,[$f_key]))));
        $this->setAttr('actions',$now_actions);
    }

    /**
     * 移除actions配置
     * @param $key
     * @param $value
     * author PengChengLei time 2022-08-10 13:30:43
     */
    protected function delActions($key,$value = null){
        $now_actions = $this->actions;
        if(in_array('*',$now_actions)){
            $now_actions = [];
        }
        //全部移除
        if(is_null($value)){
            foreach ($now_actions AS $k=>$v){
                $v = explode('.',$v);
                if(current($v) === $key){
                    unset($now_actions[$k]);
                }
            }
            $now_actions = array_values($now_actions);
        }else{
            $f_key = $this->build_temp_key($key,$value);
            $now_actions = array_diff($now_actions,[$f_key]);
        }

        if(empty($now_actions)){
            $this->resetData('actions');
        }else{
            $this->setAttr('actions',$now_actions);
        }
    }

    /**
     * 获取配置
     * @param $key
     * @param null $value
     * author PengChengLei time 2022-08-10 11:59:25
     * @return array|bool
     */
    public function getActions($key,$value = null){
        $now_actions = $this->actions;
        if(in_array('*',$now_actions)){
            return ['*'];
        }
        if(!is_null($value)){
            $f_key = $this->build_temp_key($key,$value);
            $use = in_array($f_key,$now_actions) ? true : false;
        }else{
            $use = [];
            foreach ($now_actions AS $v_actions){
                $temp_actions = explode('.',$v_actions);
                if(current($temp_actions) == $key){
                    $use[] = $v_actions;
                }
            }
        }
        return $use;
    }

    protected function build_temp_key($key,$value){
        $f_key = $key;
        if(!empty($value)){
            $f_key .= '.'.$value;
        }
        return $f_key;
    }
}