<?php
namespace app\common\lib\key;

interface KeyInterface
{
    /**
     * 获取全部配置
     * @return array
     */
    public function getAll();

    /**
     * 清空所有配置
     * author PengChengLei time 2022-08-10 14:12:03
     * @return mixed
     */
    public function cleanAll();

    /**
     * @param $name
     * author PengChengLei time 2022-08-10 14:39:13
     * @return $this
     */
    public function setName($name);

    /**
     * author PengChengLei time 2022-08-10 14:55:12
     * @return mixed|null
     */
    public function getName();

    /**
     * @param $uid
     * author PengChengLei time 2022-08-10 14:39:08
     * @return $this
     */
    public function setUid($uid);

    /**
     * author PengChengLei time 2022-08-10 14:55:04
     * @return mixed|null
     */
    public function getUid();

    /**
     * 设置秘钥备注
     * author PengChengLei time 2022-08-10 14:39:04
     * @param $Description
     * @return mixed
     */
    public function setDescription($Description);

    /**
     * author PengChengLei time 2022-08-10 14:55:17
     * @return mixed|null
     */
    public function getDescription();

    /**
     * 设置过期时间
     * @param $ExpiresAt
     * author PengChengLei time 2022-08-10 14:41:51
     * @return mixed
     */
    public function setExpiresAt($ExpiresAt);

    /**
     * author PengChengLei time 2022-08-10 14:55:22
     * @return mixed|null
     */
    public function getExpiresAt();

    /**
     * @param $key
     * @param null $value
     * author PengChengLei time 2022-08-10 14:55:27
     * @return array|bool
     */
    public function getActions($key,$value = null);

    /**
     * 提供对授权索引的访问【post|get搜索端点】
    search	Provides access to both POST and GET search endpoints on authorized indexes.
     * @return $this
     */
    public function setSearch();

    /**
     * @return $this
     */
    public function delSearch();

    /**
     * 设置文件权限
     * @param $name
        documents.add	Provides access to the add documents and update documents endpoints on authorized indexes.
        documents.get	Provides access to the get one document and get documents endpoints on authorized indexes.
        documents.delete	Provides access to the delete one document, delete all documents, and batch delete endpoints on authorized indexes.
     * @return $this
     */
    public function setDocuments($name);

    /**
     * @param null $name
     * @return $this
     */
    public function delDocuments($name = null);

    /**
     * 设置索引权限
     * @param $name
        indexes.create	Provides access to the create index endpoint.
     * @return $this
     */
    public function setIndexes($name);

    /**
     * @param null $name
     * @return $this
     */
    public function delIndexes($name = null);

    /**
     * 设置任务访问权限
     * @param $name
        tasks.get	Provides access to the get one task and get tasks endpoints. Tasks from non-authorized indexes will be omitted from the response.
     * @return $this
     */
    public function setTasks($name);

    /**
     * @param null $name
     * @return $this
     */
    public function delTasks($name = null);

    /**
     * 设置（设置权限）权限
     * @param $name
        settings.get	Provides access to the get settings endpoint and equivalents for all subroutes on authorized indexes.
        settings.update	Provides access to the update settings and reset settings endpoints and equivalents for all subroutes on authorized indexes.
     * * @return $this
     */
    public function setSettings($name);

    /**
     * @param null $name
     * @return $this
     */
    public function delSettings($name = null);

    /**
     * 设置获取统计信息权限
     * @param $name
        stats.get	Provides access to the get stats of an index endpoint and the get stats of all indexes endpoint. For the latter, non-authorized indexes are omitted from the response.
     * @return $this
     */
    public function setStats($name);

    /**
     * @param null $name
     * @return $this
     */
    public function delStats($name = null);
    /**
     * 设置转储权限
     * @param $name
        dumps.create	Provides access to the create dump endpoint. Not restricted by indexes.
     * @return $this
     */
    public function setDumps($name);

    /**
     * @param null $name
     * @return $this
     */
    public function delDumps($name = null);

    /**
     * 提供对版本端点的访问
        version	Provides access to the get Meilisearch version endpoint.
     * @return $this
     */
    public function setVersion();

    /**
     * @return $this
     */
    public function delVersion();

    /**
     * 设置对秘钥管理权限
     * @param $name
        keys.get	Provides access to the get all keys endpoint.
        keys.create	Provides access to the create key endpoint.
        keys.update	Provides access to the update key endpoint.
        keys.delete	Provides access to the delete key endpoint
     * @return $this
     */
    public function setKeys($name);

    /**
     * @param null $name
     * @return $this
     */
    public function delKeys($name = null);
    /**
     * 重置数据
     * @param string $field
     * @return $this
     */
    public function resetData($field = 'actions');
}