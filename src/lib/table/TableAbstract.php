<?php


namespace Pmeilisearch\lib\table;

/**
 * Class TableAbstract
 * @package app\common\meilisearch\table
 */
abstract class TableAbstract implements TableInterFace
{
    /**
     * 文档索引
     * @var
     */
    protected $index;

    /**
     * 文档id -字段(主键id)
     * @var
     */
    protected $PrimaryKey = 'id';

    /**
     * 文档内容
     * @var array
     */
    protected $table_data = [];

    /**
     * 文档字段
     * @var array
     */
    protected $fields = [];

    /**
     * 索引属性
     * @var array
     */
    protected $settings = [
        //返回文档中显示的字段
        'displayedAttributes'=>['*'],
        //唯一字段
        'distinctAttribute'=>null,
        //
        'faceting'=>[
            //最大返回结果数
            'maxValuesPerFacet'=>100
        ],
        //可过滤的属性 -- 包含可在查询时用作过滤器的属性的字符串数组。
        // 如果属性包含对象，您可以使用点表示法将其一个或多个键设置为此设置的值
        // 如：["release_date.year"]
        'filterableAttributes'=>[],
        //分页设置
        'pagination'=>[
            //Meilisearch 可以返回的最大结果数 maxTotalHits优先于搜索参数，例如limit和offset。
            'maxTotalHits'=>1000
        ],
        //排名规则 attribute_name 表示字段名称
        // 应用升序排序（结果按递增值排序）：attribute_name:asc
        // 应用降序排序（结果按递减值排序）：attribute_name:desc
        'rankingRules'=>[],
        //可搜索属性
        // 如果属性包含对象，您可以使用点表示法将其一个或多个键设置为此设置的值
        // 如：["release_date.year"]
        'searchableAttributes'=>['*'],
        //可排序属性
        // 如果属性包含对象，您可以使用点表示法将其一个或多个键设置为此设置的值
        // 如："author.surname"
        'sortableAttributes'=>[],
        //停用词|忽略词 -- 出现在搜索查询中时被忽略的单词列表
        'stopWords'=>[],
        //同义词 -- 类似处理的相关词列表
        // 如：[
        //      'wolverine' => ['xmen', 'logan'],
        //      'logan' => ['wolverine', 'xmen'],
        //      'wow' => ['world of warcraft']
        //     ]
        'synonyms'=>[],
        //错字容差设置
        'typoTolerance'=>[
            'enabled'=>true,//是否启用错字容错
            'minWordSizeForTypos'=>[
                'oneTypo'=>5,//容忍 1 个错字的最小字长
                'twoTypos'=>9//容忍 2 个错别字的最小字数
            ],
            'disableOnWords'=>[],//禁用错字容错功能的单词数组 -- 特殊单词禁用
            'disableOnAttributes'=>[],//禁用错字容错功能的属性数组 -- 特殊字段禁用
        ]
    ];

    /**
     * 是否开启文档字段校验
     * @var bool
     */
    protected $check_fields = true;



    public function __construct()
    {
        $this->init();
        $this->open_check();
    }

    /**
     * 初始化
     * author PengChengLei time 2022-07-20 14:45:21
     * @return mixed|void
     */
    public function init()
    {
        $this->id = 'id';
        $this->index = '';
        $this->table_data = [];
        return $this;
    }

    public function open_check(){
        $this->check_fields = true;
        return $this;
    }

    public function close_check(){
        $this->check_fields = false;
        return $this;
    }

    public function setPrimaryKey(string $PrimaryKey)
    {
        $this->PrimaryKey = $PrimaryKey;
        return $this;
    }

    public function getPrimaryKey()
    {
        return $this->PrimaryKey;
    }

    public function setIndex($index)
    {
        $this->index = $index;
        return $this;
    }

    public function getIndex()
    {
        return $this->index;
    }

    public function setData(array $data)
    {
        $new_data = $data;
        //开启字段验证
        if($this->check_fields){
            $new_data = [];
            foreach ($this->fields AS $v_fields){
                if(isset($data[$v_fields])){
                    $new_data[$v_fields] = $data[$v_fields];
                }
            }
        }
        $this->table_data = $new_data;
        return $this;
    }

    /**
     * author PengChengLei time 2022-07-20 14:27:06
     * @return array
     */
    public function getData()
    {
        return $this->table_data;
    }

    public function checkData($type = '@all')
    {
        switch ($type){
            case 'id':
                $result = !empty($this->id);
                break;
            case 'index':
                $result = !empty($this->index);
                break;
            case 'id_index':
                $result = !empty($this->id) && !empty($this->index);
                break;
            case '@all':
            default:
                $result = !empty($this->id) && !empty($this->index) && !empty($this->table_data);
                //必须包含文档id
                if(!isset($this->table_data[$this->getPrimaryKey()])){
                    $result = false;
                }
                break;
        }
        return $result;
    }

    public function getSetting()
    {
        return $this->settings;
    }

    public function setSettingAll(array $option)
    {
        foreach ($option AS $k_option=>$v_option){
            $this->setSetting($k_option,$v_option);
        }
    }

    public function setSetting($attribute, $option)
    {
        $this->settings[$attribute] = $option;
    }
}