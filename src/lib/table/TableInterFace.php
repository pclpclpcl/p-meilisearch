<?php


namespace Pmeilisearch\lib\table;


interface TableInterFace
{
    /**
     * 初始化
     * author PengChengLei time 2022-07-20 14:43:48
     * @return $this
     */
    public function init();
    /**
     * 设置文档id
     * author PengChengLei time 2022-07-20 14:16:56
     * @param string $id
     * @return $this
     */
    public function setPrimaryKey(string $id);

    /**
     * 获取文档id
     * author PengChengLei time 2022-07-20 14:17:08
     * @return int
     */
    public function getPrimaryKey();

    /**
     * 设置索引id
     * author PengChengLei time 2022-07-20 14:17:14
     * @param string|int $index
     * @return $this
     */
    public function setIndex($index);

    /**
     * 获取索引id
     * author PengChengLei time 2022-07-20 14:17:31
     * @return mixed
     */
    public function getIndex();

    /**
     * 设置文档字段 -- 批量
     * author PengChengLei time 2022-07-20 14:17:39
     * @param array $data
     * @return $this
     */
    public function setData(array $data);

    /**
     * 获取文档字段
     * author PengChengLei time 2022-07-20 14:18:32
     * @return array
     */
    public function getData();

    /**
     * 校验数据是否有内容
     * author PengChengLei time 2022-07-20 15:01:42
     * @param string $type
     * @return bool
     */
    public function checkData($type = '@all');

    /**
     * 获取文档配置
     * author PengChengLei time 2022-07-28 15:23:39
     * @return mixed
     */
    public function getSetting();

    /**
     * 设置文档配置(批量设置)
     * author PengChengLei time 2022-07-28 15:24:02
     * @param array $option
     * @return mixed
     */
    public function setSettingAll(array $option);

    /**
     * 设置文档配置(指定替换内容)
     * @param $attribute
     * @param $option
     * author PengChengLei time 2022-07-28 15:25:37
     * @return mixed
     */
    public function setSetting($attribute,$option);
}