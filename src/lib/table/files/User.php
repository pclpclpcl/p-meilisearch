<?php


namespace Pmeilisearch\lib\table\files;


use Pmeilisearch\lib\table\TableAbstract;

class User extends TableAbstract
{
    protected $fields = [
        'id','name','userid','avatar'
    ];
}