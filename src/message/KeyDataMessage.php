<?php


namespace Pmeilisearch\message;


use Pmeilisearch\lib\ArrayObj;

/**
 * Class TaskDataMessage
 * @package app\common\meilisearch\message
 * @property string|null name
 * @property string|null description
 * @property string key
 * @property string uid
 * @property array actions
 * @property array indexes
 * @property string expiresAt
 * @property string createdAt
 * @property string updatedAt
 *
 *
 * 示例：
        "name": null,
        "description": "Manage documents: Products/Reviews API key",
        "key": "d0552b41536279a0ad88bd595327b96f01176a60c2243e906c52ac02375f9bc4",
        "uid": "6062abda-a5aa-4414-ac91-ecd7944c0f8d",
        "actions": [
        "documents.add"
        ],
        "indexes": [
        "products"
        ],
        "expiresAt": "2021-11-13T00:00:00Z",
        "createdAt": "2021-11-12T10:00:00Z",
        "updatedAt": "2021-11-12T10:00:00Z"
 */
class KeyDataMessage extends ArrayObj
{
    protected function init()
    {
        $this->close_check();
    }
}