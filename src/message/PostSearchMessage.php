<?php


namespace Pmeilisearch\message;


use Pmeilisearch\lib\ArrayObj;

/**
 * Class PostSearchMessage
 * @package meilisearch\message
 * @property int offset
 * @property int limit
 * @property string|null filter 按属性值过滤查询
 * @property array|null facets 返回与每个给定方面的当前搜索查询匹配的文档数，设置时facets，搜索结果对象包含facetDistribution显示每个指定方面的剩余候选数的字段。
 * @property array  attributesToRetrieve 返回文档中显示的属性 默认：["*"]
 * @property int|null attributesToCrop 必须裁剪其值的属性
 * @property int cropLength 单词中裁剪值的最大长度
 * @property string cropMarker 字符串标记作物边界
 * @property string|null attributesToHighlight 突出显示属性中包含的匹配项
 * @property string highlightPreTag 在突出显示的术语开头插入的字符串 "<em>"
 * @property string highlightPostTag 在突出显示的术语末尾插入的字符串 "<em>"
 * @property bool showMatchesPosition 返回匹配条件位置
 * @property string|null sort 按属性值对搜索结果进行排序
 *
 * 参考链接：https://docs.meilisearch.com/reference/api/search.html#query-q
 */
class PostSearchMessage extends ArrayObj
{
    /**
     * 需要校验的字段
     * @var array
     */
    protected $check_field = [
        'offset','limit','filter','facets','attributesToRetrieve','attributesToCrop','cropLength','cropMarker','attributesToHighlight',
        'highlightPreTag','highlightPostTag','showMatchesPosition','sort'
    ];

    protected function init()
    {
        $this->close_check();
    }
}