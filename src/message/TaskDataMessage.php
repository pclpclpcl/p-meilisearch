<?php


namespace Pmeilisearch\message;


use Pmeilisearch\lib\ArrayObj;

/**
 * Class TaskDataMessage
 * @package app\common\meilisearch\message
 * @property int taskUid
 * @property int|string indexUid
 * @property string status
 * @property string type
 * @property string enqueuedAt
 *
 *
 * 示例：
    "taskUid" => 1
    "indexUid" => "user"
    "status" => "enqueued"
    "type" => "documentAdditionOrUpdate"
    "enqueuedAt" => "2022-07-20T07:01:03.5743302Z"
 */
class TaskDataMessage extends ArrayObj
{
    protected function init()
    {
        $this->close_check();
    }
}